backendapp.templatetags package
===============================

Submodules
----------

backendapp.templatetags.customFilters module
--------------------------------------------

.. automodule:: backendapp.templatetags.customFilters
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: backendapp.templatetags
   :members:
   :undoc-members:
   :show-inheritance:

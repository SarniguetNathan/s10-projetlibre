backendapp.migrations package
=============================

Submodules
----------

backendapp.migrations.0001\_initial module
------------------------------------------

.. automodule:: backendapp.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.migrations.0002\_auto\_20200402\_1436 module
-------------------------------------------------------

.. automodule:: backendapp.migrations.0002_auto_20200402_1436
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.migrations.0003\_auto\_20200402\_1535 module
-------------------------------------------------------

.. automodule:: backendapp.migrations.0003_auto_20200402_1535
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: backendapp.migrations
   :members:
   :undoc-members:
   :show-inheritance:

invoice package
===============

Submodules
----------

invoice.asgi module
-------------------

.. automodule:: invoice.asgi
   :members:
   :undoc-members:
   :show-inheritance:

invoice.settings module
-----------------------

.. automodule:: invoice.settings
   :members:
   :undoc-members:
   :show-inheritance:

invoice.urls module
-------------------

.. automodule:: invoice.urls
   :members:
   :undoc-members:
   :show-inheritance:

invoice.wsgi module
-------------------

.. automodule:: invoice.wsgi
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: invoice
   :members:
   :undoc-members:
   :show-inheritance:

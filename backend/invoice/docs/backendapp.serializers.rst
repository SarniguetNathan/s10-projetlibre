backendapp.serializers package
==============================

Submodules
----------

backendapp.serializers.accountLineSerializer module
---------------------------------------------------

.. automodule:: backendapp.serializers.accountLineSerializer
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.serializers.addressSerializer module
-----------------------------------------------

.. automodule:: backendapp.serializers.addressSerializer
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.serializers.clientSerializer module
----------------------------------------------

.. automodule:: backendapp.serializers.clientSerializer
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.serializers.invoiceSerializer module
-----------------------------------------------

.. automodule:: backendapp.serializers.invoiceSerializer
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.serializers.itemLineSerializer module
------------------------------------------------

.. automodule:: backendapp.serializers.itemLineSerializer
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.serializers.itemSerlializer module
---------------------------------------------

.. automodule:: backendapp.serializers.itemSerlializer
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.serializers.userSerializer module
--------------------------------------------

.. automodule:: backendapp.serializers.userSerializer
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: backendapp.serializers
   :members:
   :undoc-members:
   :show-inheritance:

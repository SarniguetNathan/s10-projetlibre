backendapp package
==================

Subpackages
-----------

.. toctree::

   backendapp.migrations
   backendapp.models
   backendapp.serializers
   backendapp.templatetags
   backendapp.views

Submodules
----------

backendapp.admin module
-----------------------

.. automodule:: backendapp.admin
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.apps module
----------------------

.. automodule:: backendapp.apps
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.cors module
----------------------

.. automodule:: backendapp.cors
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.models module
------------------------

.. automodule:: backendapp.models
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.serializers module
-----------------------------

.. automodule:: backendapp.serializers
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.tests module
-----------------------

.. automodule:: backendapp.tests
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.views module
-----------------------

.. automodule:: backendapp.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: backendapp
   :members:
   :undoc-members:
   :show-inheritance:

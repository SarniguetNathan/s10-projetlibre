.. projet_libre documentation master file, created by
   sphinx-quickstart on Sat Apr  4 15:47:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to invoice's documentation!
========================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   modules/views.rst
   modules/models.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

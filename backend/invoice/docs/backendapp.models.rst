backendapp.models package
=========================

Submodules
----------

backendapp.models.AccountLine module
------------------------------------

.. automodule:: backendapp.models.AccountLine
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.models.Address module
--------------------------------

.. automodule:: backendapp.models.Address
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.models.Client module
-------------------------------

.. automodule:: backendapp.models.Client
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.models.Invoice module
--------------------------------

.. automodule:: backendapp.models.Invoice
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.models.Item module
-----------------------------

.. automodule:: backendapp.models.Item
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.models.ItemLine module
---------------------------------

.. automodule:: backendapp.models.ItemLine
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.models.User module
-----------------------------

.. automodule:: backendapp.models.User
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: backendapp.models
   :members:
   :undoc-members:
   :show-inheritance:

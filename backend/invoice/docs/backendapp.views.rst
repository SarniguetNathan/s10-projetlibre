backendapp.views package
========================

Submodules
----------

backendapp.views.accountLineView module
---------------------------------------

.. automodule:: backendapp.views.accountLineView
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.views.addressView module
-----------------------------------

.. automodule:: backendapp.views.addressView
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.views.clientView module
----------------------------------

.. automodule:: backendapp.views.clientView
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.views.invoiceView module
-----------------------------------

.. automodule:: backendapp.views.invoiceView
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.views.itemLineView module
------------------------------------

.. automodule:: backendapp.views.itemLineView
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.views.itemView module
--------------------------------

.. automodule:: backendapp.views.itemView
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.views.pdfView module
-------------------------------

.. automodule:: backendapp.views.pdfView
   :members:
   :undoc-members:
   :show-inheritance:

backendapp.views.userView module
--------------------------------

.. automodule:: backendapp.views.userView
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: backendapp.views
   :members:
   :undoc-members:
   :show-inheritance:

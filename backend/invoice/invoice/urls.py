"""invoice URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from django.urls import include, path
from rest_framework import routers
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from backendapp import views

router = routers.DefaultRouter()
#router.register(r'item', ItemView.get)

urlpatterns = [
    #path('', include(router.urls)),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    path('clients/<int:id>/', views.ClientDetails.as_view()),
    path('clients/', views.ClientDetails.as_view()),

    path('invoices/<int:id>/', views.InvoiceDetails.as_view()),
    path('invoices/', views.InvoiceDetails.as_view()),

    path('addresses/<int:id>/', views.AddressDetails.as_view()),
    path('addresses/', views.AddressDetails.as_view()),

    path('accountlines/<int:id>/', views.AccountLineDetails.as_view()),
    path('accountlines/', views.AccountLineDetails.as_view()),

    path('itemlines/<int:id>/', views.ItemLineDetails.as_view()),
    path('itemlines/', views.ItemLineDetails.as_view()),

    path('items/<int:id>/', views.ItemDetails.as_view()),
    path('items/', views.ItemDetails.as_view()),

    path('users/', views.UserDetails.as_view()),

    path('clients/<int:id>/invoices/', views.InvoiceClient.as_view()),
    path('exportinvoice/<int:id>/', views.GeneratePdf.as_view()),
]



from rest_framework import serializers
from rest_framework.serializers import Serializer

from backendapp.models import Client, User


class UserSerializer(serializers.ModelSerializer):
    """
        Serializer for the User Model.
    """

    company_name = serializers.CharField(allow_blank=True, allow_null=True, trim_whitespace=True)
    first_name = serializers.CharField(allow_blank=True, allow_null=True, trim_whitespace=True)
    last_name = serializers.CharField(allow_blank=True, allow_null=True, trim_whitespace=True)



    class Meta:
        model = User
        exclude = ['password']
        depth = 1
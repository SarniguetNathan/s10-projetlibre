from rest_framework import serializers

from backendapp.models import Client, Address


class AddressSerializer(serializers.ModelSerializer):
    """
        Serializer for the Address Model.
    """
    class Meta:
        model = Address
        fields = '__all__'

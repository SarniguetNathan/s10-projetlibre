from rest_framework import serializers
from backendapp.models import ItemLine


class ItemLineSerializer(serializers.ModelSerializer):
    """
        Serializer for the ItemLine Model.
    """
    class Meta:
        model = ItemLine
        fields = '__all__'
        depth = 1
from rest_framework import serializers

from backendapp.models import Item


class ItemSerializer(serializers.ModelSerializer):
    """
            Serializer for the Item Model.
    """
    class Meta:
        model = Item
        exclude = ['user',]

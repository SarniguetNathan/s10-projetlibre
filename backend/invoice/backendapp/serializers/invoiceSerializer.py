from rest_framework import serializers

from backendapp.models import Invoice, ItemLine
from backendapp.serializers import ItemLineSerializer


class InvoiceSerializer(serializers.ModelSerializer):
    """
        Serializer for the Invoice Model.
    """
    item_lines = serializers.SerializerMethodField()

    class Meta:
        model = Invoice
        # exclude = ['user', 'client_JSON', 'user_JSON']
        fields = ['id',
                  'invoice_number',
                  'creation_date',
                  'effective_date',
                  'client',
                  'order_number',
                  'reduction',
                  'client_JSON',
                  'user_JSON',
                  'interest_rate',
                  'payment_delay',
                  'description',
                  'item_lines',
                  'user']
        depth = 3

    def get_item_lines(self, obj):
        item_lines = ItemLine.objects.all().filter(invoice_id=obj.id)
        return ItemLineSerializer(item_lines, many=True).data

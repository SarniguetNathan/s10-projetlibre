from rest_framework import serializers

from backendapp.models import AccountLine, Item, ItemLine
from backendapp.serializers import ItemSerializer, ItemLine, ItemLineSerializer



class AccountLineSerializer(serializers.ModelSerializer):
    """
        Serializer for the AccountLine Model.
    """
    item_lines = serializers.SerializerMethodField()
    class Meta:
        model = AccountLine
        # exclude = ['user', ]
        fields = [
            'id',
            'description',
            'account_line_date',
            'client',
            'invoice',
            'pre_tax_total',
            'tax_amount',
            'item_lines']
        depth = 1

    def get_item_lines(self, obj):
        item_lines = ItemLine.objects.all().filter(account_line_id=obj.id)
        return ItemLineSerializer(item_lines, many=True).data

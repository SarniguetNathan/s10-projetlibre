from rest_framework import serializers

from backendapp.models import Client


class ClientSerializer(serializers.ModelSerializer):
    """
            Serializer for the Client Model.
    """
    class Meta:
        model = Client
        depth = 1
        exclude = ['user',]

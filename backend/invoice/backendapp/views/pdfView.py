import json

from django.http import HttpResponse
from django.template.loader import get_template
from django.utils.datetime_safe import datetime
from django.views.generic import View

from backendapp.models import Invoice
from backendapp.serializers import InvoiceSerializer
from backendapp.utils.renderpdf import render_to_pdf
from backendapp.serializers.userSerializer import UserSerializer
from backendapp.serializers import ClientSerializer


class GeneratePdf(View):
    """
    Endpoint for generating the pdf
    """
    def get(self, request, id, *args, **kwargs):
        template = get_template('../templates/pdf_template.html')
        invoice_obj = Invoice.objects.get(id=id, user__id = request.user.id)
        invoice = InvoiceSerializer(invoice_obj).data
        pre_tax_total = 0
        taxed_total = 0
        tax_amount = 0
        after_reduction = 0
        for item_line in invoice['item_lines']:
            pre_tax_total += item_line['item']['pre_tax_price'] * item_line['quantity']
            tax_amount += item_line['item']['pre_tax_price'] * item_line['quantity'] * item_line['item']['tax']
        print(invoice_obj.user_JSON)
        if(invoice_obj.user_JSON != "" and invoice_obj.user_JSON != None):
            invoice['user'] = UserSerializer(json.loads(invoice_obj.user_JSON)).data
        if (invoice_obj.client_JSON != "" and invoice_obj.client_JSON != None):
            invoice['client'] = ClientSerializer(json.loads(invoice_obj.client_JSON)).data

        taxed_total = pre_tax_total + tax_amount

        if invoice['reduction'] != 0:
            after_reduction = taxed_total - invoice['reduction']
        context = {'pre_tax_total': round(pre_tax_total,2),
                   'taxed_total': round(taxed_total,2),
                   'tax_amount' : round(tax_amount,2),
                   'after_reduction': round(after_reduction,2),
                   'user': invoice['user']}
        context.update(invoice)
        html = template.render(context)
        pdf = render_to_pdf('../templates/pdf_template.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "Invoice_%s.pdf" % ("12341231")
            content = "inline; filename='%s'" % (filename)
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" % (filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")

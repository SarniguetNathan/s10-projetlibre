import json

from django.http import Http404
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from backendapp.models import Client, User, Address, Invoice
from backendapp.serializers import InvoiceSerializer, ClientSerializer
from backendapp.serializers.userSerializer import UserSerializer


class InvoiceList(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        invoices = Invoice.objects.all().filter(user__id = request.user.id).distinct()
        serializer = InvoiceSerializer(invoices, many=True)
        return Response(serializer.data)

class InvoiceClient(APIView):
    """
        Return the invoices from the same client
    """
    def get(self, request, id):
        invoices = Invoice.objects.all().filter(user__id=request.user.id, client__id=id).distinct()
        serializer = InvoiceSerializer(invoices, many=True)
        return Response(serializer.data)


class InvoiceDetails(APIView):
    """
        Invoice endpoints with :
        - get : get by id
        - delete : delete by id
        - put : modify
        - post : create
    """
    permission_classes = (IsAuthenticated,)

    def get_object(self, id):
        try:
            return Invoice.objects.get(id=id)
        except Invoice.DoesNotExist:
            raise Http404

    def retrieve(self, request, pk=None):
        pass

    def get(self, request, id=-1, format=None):
        if id == -1:
            invoices = Invoice.objects.all().filter(user__id=request.user.id).distinct()
            serializer = InvoiceSerializer(invoices, many=True)
            return Response(serializer.data)
        else:
            invoice = self.get_object(id)
            serializer = InvoiceSerializer(invoice)
            if invoice.user.id == request.user.id :
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def delete(self, request, id, format=None):
        invoice = self.get_object(id)
        if invoice.user.id == request.user.id :
            invoice.delete()
            return Response(status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


    def put(self, request, id, format=None):
        invoice = self.get_object(id)
        serializer = InvoiceSerializer(invoice, data=request.data)
        client_id = self.request.data.pop('client_id')
        client = Client.objects.get(id=client_id)
        user = User.objects.get(id=request.user.id)
        client_string = json.dumps(ClientSerializer(client).data)
        user_string = json.dumps(UserSerializer(user).data)
        if invoice.user.id == request.user.id :
            if serializer.is_valid():
                invoice = serializer.save(user=user, client=client, client_JSON=client_string, user_JSON=user_string)
                serializer = InvoiceSerializer(invoice)
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_401_UNAUTHORIZED)

    def post(self, request):
        serializer = InvoiceSerializer(data=request.data)
        client_id = self.request.data.pop('client_id')
        client = Client.objects.get(id=client_id)
        user = User.objects.get(id = request.user.id)
        client_string = json.dumps(ClientSerializer(client).data)
        user_string = json.dumps(UserSerializer(user).data)
        print(str(client))
        if serializer.is_valid() and client:
            invoice = serializer.save(user=user, client=client, client_JSON=client_string, user_JSON=user_string)
            serializer = InvoiceSerializer(invoice)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
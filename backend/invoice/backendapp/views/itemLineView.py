import json

from django.http import Http404
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from backendapp.models import Client, User, Address, Invoice, AccountLine, ItemLine, Item
from backendapp.serializers import ClientSerializer, AccountLineSerializer, ItemLineSerializer, ItemSerializer


class ItemLineList(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        itemLines = ItemLine.objects.all().filter(account_line__user__id = request.user.id).distinct()
        serializer = ItemLineSerializer(itemLines, many=True)
        return Response(serializer.data)


class ItemLineDetails(APIView):
    """
        ItemLine endpoints with :
        - get : get by id
        - delete : delete by id
        - put : modify
        - post : create
    """
    permission_classes = (IsAuthenticated,)

    def get_object(self, id):
        try:
            return ItemLine.objects.get(id=id)
        except ItemLine.DoesNotExist:
            raise Http404

    def retrieve(self, request, pk=None):
        pass

    def get(self, request, id=-1, format=None):
        if id == -1:
            itemLines = ItemLine.objects.all().filter(account_line__user__id=request.user.id).distinct()
            serializer = ItemLineSerializer(itemLines, many=True)
            return Response(serializer.data)
        else :
            item_line = self.get_object(id)
            if item_line.account_line.user.id == request.user.id:
                serializer = ItemLineSerializer(item_line)
                return Response(serializer.data)
            return Response(status=status.HTTP_400_BAD_REQUEST)


    def delete(self, request, id, format=None):
        item_line = self.get_object(id)
        if item_line.account_line.user.id == request.user.id:
            item_line.delete()
            return Response(status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


    def put(self, request, id, format=None):
        item_line = self.get_object(id)
        serializer = ItemLineSerializer(item_line, data=request.data)
        item_id = self.request.data.pop('item_id')
        item = Item.objects.get(id=item_id)
        invoice_id = self.request.data.pop('invoice_id')
        invoice = Invoice.objects.get(id=invoice_id)
        account_line_id = self.request.data.pop('account_line_id')
        account_line = AccountLine.objects.get(id=account_line_id)
        item_string = json.dumps(ItemSerializer(item).data)
        if account_line.user.id == request.user.id:
            if serializer.is_valid():
                item_line = serializer.save(invoice=invoice, account_line=account_line, item=item, item_JSON = item_string)
                serializer = ItemLineSerializer(item_line)
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        serializer = ItemLineSerializer(data=request.data)
        item_id = self.request.data.pop('item_id')
        item = Item.objects.get(id=item_id)
        item_string = json.dumps(ItemSerializer(item).data)
        invoice_id = self.request.data.pop('invoice_id')
        invoice = None
        if invoice_id != -1:
            invoice = Invoice.objects.get(id=invoice_id)
        account_line = None
        account_line_id = self.request.data.pop('account_line_id')
        if account_line_id != -1:
            account_line = AccountLine.objects.get(id = account_line_id)
        if serializer.is_valid():
            item_line = serializer.save(invoice=invoice, account_line=account_line, item=item, item_JSON=item_string)
            serializer = ItemLineSerializer(item_line)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

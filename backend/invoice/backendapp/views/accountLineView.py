from django.http import Http404
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from backendapp.models import Client, User, Address, Invoice, AccountLine
from backendapp.serializers import ClientSerializer, AccountLineSerializer




class AccountLineDetails(APIView):
    """
    AccountLine endpoints with :
    - get : get by id
    - delete : delete by id
    - put : modify
    - post : create
    """
    permission_classes = (IsAuthenticated,)

    def get_object(self, id):

        try:
            return AccountLine.objects.get(id=id)
        except AccountLine.DoesNotExist:
            raise Http404

    def retrieve(self, request, pk=None):
        pass

    def get(self, request, id=-1, format=None):
        if id == -1:
            accountLines = AccountLine.objects.all().filter(user__id=request.user.id).order_by(
                '-account_line_date').distinct()
            serializer = AccountLineSerializer(accountLines, many=True)
            return Response(serializer.data)
        else :
            account_line = self.get_object(id)
            if account_line.user.id == request.user.id:
                serializer = AccountLineSerializer(account_line)
                return Response(serializer.data)
            return Response(status=status.HTTP_400_BAD_REQUEST)


    def delete(self, request, id, format=None):
        account_line = self.get_object(id)
        if account_line.user.id == request.user.id:
            account_line.delete()
            return Response(status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


    def put(self, request, id, format=None):
        account_line = self.get_object(id)
        serializer = AccountLineSerializer(account_line, data=request.data)
        client_id = self.request.data.pop('client_id')
        client = Client.objects.get(id=client_id)
        user = User.objects.get(id=request.user.id)
        invoice_id = self.request.data.pop('invoice_id')
        invoice = Invoice.objects.get(id=invoice_id)
        if account_line.user.id == request.user.id:
            if serializer.is_valid():
                account_line = serializer.save(user=user, client=client, invoice=invoice)
                serializer = AccountLineSerializer(account_line)
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        serializer = AccountLineSerializer(data=request.data)
        client_id = self.request.data.pop('client_id')
        client = Client.objects.get(id=client_id)
        user = User.objects.get(id=request.user.id)
        invoice_id = self.request.data.pop('invoice_id')
        invoice = None
        if invoice_id != -1:
            invoice = Invoice.objects.get(id=invoice_id)
        if serializer.is_valid():
            account_line = serializer.save(user=user, client=client, invoice=invoice)
            serializer = AccountLineSerializer(account_line)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

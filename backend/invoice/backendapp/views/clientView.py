import json

from django.http import Http404
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from backendapp.models import Client, User, Address, Invoice
from backendapp.serializers import ClientSerializer


class ClientList(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        clients = Client.objects.all().filter(user__id = request.user.id).distinct()
        serializer = ClientSerializer(clients, many=True)
        return Response(serializer.data)

class ClientDetails(APIView):
    """
        Client endpoints with :
        - get : get by id
        - delete : delete by id
        - put : modify
        - post : create
    """
    permission_classes = (IsAuthenticated,)

    def get_object(self, id):
        try:
            return Client.objects.get(id=id)
        except Client.DoesNotExist:
            raise Http404

    def retrieve(self, request, pk=None):
        pass

    def get(self, request, id=-1, format=None):
        if id == -1 :
            clients = Client.objects.all().filter(user__id=request.user.id).distinct()
            serializer = ClientSerializer(clients, many=True)
            return Response(serializer.data)
        else:
            client = self.get_object(id)
            if client.user.id == request.user.id:
                serializer = ClientSerializer(client)
                return Response(serializer.data)
            return Response(status=status.HTTP_400_BAD_REQUEST)


    def delete(self, request, id, format=None):
        client = self.get_object(id)
        if client.user.id == request.user.id:
            client.delete()
            return Response(status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


    def put(self, request, id, format=None):
        client = self.get_object(id)
        serializer = ClientSerializer(client, data=request.data)
        address_id = self.request.data.pop('address_id')
        address = Address.objects.get(id=address_id)
        invoice_address_id = self.request.data.pop('invoice_address_id')
        invoice_address = Address.objects.get(id = invoice_address_id)
        user = User.objects.get(id=request.user.id)
        if client.user.id == request.user.id:
            if serializer.is_valid():
                client = serializer.save(address=address, invoice_address=invoice_address, user=user)
                serializer = ClientSerializer(client)
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        serializer = ClientSerializer(data=request.data)
        address_id = self.request.data.pop('address_id')
        address = Address.objects.get(id = address_id)
        invoice_address_id = self.request.data.pop('invoice_address_id')
        invoice_address = Address.objects.get(id = invoice_address_id)
        user = User.objects.get(id=request.user.id)
        if serializer.is_valid():
            client = serializer.save(address=address, invoice_address=invoice_address, user=user)
            serializer = ClientSerializer(client)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

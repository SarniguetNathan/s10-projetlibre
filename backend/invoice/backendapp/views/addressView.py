import json

from django.http import Http404
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from backendapp.models import Client, User, Address, Invoice
from backendapp.serializers import ClientSerializer
from backendapp.serializers.addressSerializer import AddressSerializer


class AddressDetails(APIView):
    """
        Address endpoints with :
        - put : modify
        - post : create
    """
    permission_classes = (IsAuthenticated,)

    def get_object(self, id):
        try:
            return Address.objects.get(id=id)
        except Address.DoesNotExist:
            raise Http404

    def retrieve(self, request, pk=None):
        pass

    def put(self, request, id, format=None):
        address = self.get_object(id)
        serializer = AddressSerializer(address, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        serializer = AddressSerializer(data=request.data)
        if serializer.is_valid():
            address = serializer.save()
            serializer = AddressSerializer(address)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

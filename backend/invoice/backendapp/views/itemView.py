import json

from django.http import Http404
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from backendapp.models import Client, User, Address, Invoice, Item
from backendapp.serializers import ClientSerializer, ItemSerializer


class ItemList(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        items = Item.objects.all().filter(user__id = request.user.id).distinct()
        serializer = ItemSerializer(items, many=True)
        return Response(serializer.data)

class ItemDetails(APIView):
    """
        Item endpoints with :
        - get : get by id
        - delete : delete by id
        - put : modify
        - post : create
    """
    permission_classes = (IsAuthenticated,)

    def get_object(self, id):
        try:
            return Item.objects.get(id=id)
        except Item.DoesNotExist:
            raise Http404

    def retrieve(self, request, pk=None):
        pass

    def get(self, request, id=-1, format=None):
        if id == -1:
            items = Item.objects.all().filter(user__id=request.user.id).distinct()
            serializer = ItemSerializer(items, many=True)
            return Response(serializer.data)
        else:
            item = self.get_object(id)
            if item.user.id == request.user.id:
                serializer = ItemSerializer(item)
                return Response(serializer.data)
            return Response(status=status.HTTP_400_BAD_REQUEST)


    def delete(self, request, id, format=None):
        item = self.get_object(id)
        if item.user.id == request.user.id:
            item.delete()
            return Response(status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


    def put(self, request, id, format=None):
        item = self.get_object(id)
        serializer = ItemSerializer(item, data=request.data)
        if item.user.id == request.user.id:
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        serializer = ItemSerializer(data=request.data)
        user = User.objects.get(id=request.user.id)
        if serializer.is_valid():
            item = serializer.save(user=user)
            serializer = ItemSerializer(item)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

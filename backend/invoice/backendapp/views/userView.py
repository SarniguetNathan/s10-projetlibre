import json

from django.http import Http404
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response

from backendapp.models import Client, User, Address, Invoice
from backendapp.serializers import ClientSerializer
from backendapp.serializers.userSerializer import UserSerializer


class UserDetails(APIView):
    """
        User endpoints with :
        - get : get by id
        - delete : delete by id
        - put : modify
        - post : create
    """
    permission_classes = (IsAuthenticated,)

    def get_object(self, id):
        try:
            return User.objects.get(id=id)
        except User.DoesNotExist:
            raise Http404

    def retrieve(self, request, pk=None):
        pass

    def get(self, request, format=None):
        user = self.get_object(request.user.id)
        serializer = UserSerializer(user)
        app_json = str(json.dumps(serializer.data))
        print(app_json)
        print(UserSerializer(json.loads(app_json)).data)
        return Response(serializer.data)


    def delete(self, request, id, format=None):
        user = self.get_object(id)
        if user.id == request.user.id:
            user.delete()
            return Response(status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


    def put(self, request, id, format=None):
        user = self.get_object(id)
        serializer = UserSerializer(user, data=request.data)
        address_id = self.request.data.pop('address_id')
        address = Address.objects.get(id=address_id)
        insurance_address_id = self.request.data.pop('insurance_address_id')
        insurance_address = Address.objects.get(id = insurance_address_id)
        user = User.objects.get(id=request.user.id)
        if user.id == request.user.id:
            if serializer.is_valid():
                user = serializer.save(address=address, insurance_address=insurance_address)
                serializer = UserSerializer(user)
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)

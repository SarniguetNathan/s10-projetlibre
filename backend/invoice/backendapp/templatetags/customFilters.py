from django import template

register = template.Library()

@register.filter
def multiply(value, arg):
    """
        multiply tag for the invoice template
    """
    return round(value * arg,2)
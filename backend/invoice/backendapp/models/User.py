from django.contrib.auth.models import AbstractUser, PermissionsMixin
from django.db import models
from backendapp.models import Address


class User(AbstractUser):
    """
        model for the used
    """
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=50, null=True, default="test")
    last_name = models.CharField(max_length=50, null=True, default="test")
    company_name = models.CharField(max_length=50, null=True)
    address = models.ForeignKey(
        Address,
        related_name='user_address',
        on_delete=models.CASCADE,
        null=True,
    )
    social_denomination = models.CharField(max_length=50, null=True)
    siret = models.IntegerField(null=True)
    is_merchant = models.BooleanField(null=True)
    RCS = models.CharField(max_length=50, null=True)
    RCS_city = models.CharField(max_length=50, null=True)
    is_artisan = models.BooleanField(null=True)
    artisan_number = models.IntegerField(null=True)
    company_type = models.CharField(max_length=50, null=True)
    capital = models.IntegerField(null=True)
    email = models.CharField(max_length=50)
    password = models.CharField(max_length=150)
    TVA_number = models.IntegerField(null=True)
    is_association_or_gestion = models.BooleanField(default=False)
    is_BTP = models.BooleanField(default=False)
    is_insurance = models.BooleanField(default=False)
    insurance_mention = models.CharField(max_length=50, null=True)
    insurance_address = models.ForeignKey(
        Address,
        related_name='insurance_address',
        on_delete=models.CASCADE,
        null=True,
    )
    geographic_cover = models.CharField(max_length=50, null=True)
    def __str__(self):
        return self.email
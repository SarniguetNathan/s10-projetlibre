from django.db import models

from backendapp.models import User, AccountLine


class Item(models.Model):
    """
        model for the items
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    ref = models.IntegerField()
    pre_tax_price = models.FloatField()
    tax = models.FloatField()
    description = models.CharField(max_length=150)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )


    def __str__(self):
        return self.name
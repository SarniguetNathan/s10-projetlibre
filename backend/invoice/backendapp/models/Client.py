from django.db import models

from backendapp.models import Address
from backendapp.models import User


class Client(models.Model):
    """
            model for the clients
    """
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=50, null=True)
    last_name = models.CharField(max_length=50, null=True)
    company_name = models.CharField(max_length=50, null=True)
    is_company = models.BooleanField(default=True)
    address = models.ForeignKey(
        Address,
        related_name='client_address',
        on_delete=models.CASCADE,
        null=True
    )
    invoice_address = models.ForeignKey(
        Address,
        related_name='invoice_address',
        on_delete=models.CASCADE,
        null=True
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )
    objects = models.Manager()
    def __str__(self):
        return self.first_name + " " + self.last_name
from django.db import models

from backendapp.models import Invoice
from backendapp.models.Client import Client
from backendapp.models.User import User


class AccountLine(models.Model):
    """
        model for the account lines
    """
    id = models.AutoField(primary_key=True)
    description = models.CharField(max_length=150)
    account_line_date = models.DateField()
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        null=True
    )
    invoice = models.ForeignKey(
        Invoice,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    pre_tax_total = models.FloatField()
    tax_amount = models.FloatField()

    def __str__(self):
        return self.description

from django.db import models
from rest_framework.fields import JSONField

from backendapp.models import Client
from backendapp.models import User


class Invoice(models.Model):
    """
        model for the invoices
    """
    id = models.AutoField(primary_key=True)
    invoice_number = models.CharField(max_length=50)
    creation_date = models.DateField()
    effective_date = models.DateField()
    client = models.ForeignKey(
        Client,
        related_name='client',
        on_delete=models.CASCADE,
        null=True
    )
    client_JSON = models.CharField(max_length=1000, default="")
    order_number = models.CharField(max_length=50)
    reduction = models.FloatField()
    pay_date = models.DateTimeField(null=True)
    interest_rate = models.FloatField()
    payment_delay = models.IntegerField()

    user = models.ForeignKey(
        User,
        related_name='user',
        on_delete=models.CASCADE,
        null=True
    )
    user_JSON = models.CharField(max_length=1000, default="")
    description = models.CharField(max_length=200)
    objects = models.Manager()

    def __str__(self):
        return self.invoice_number

from django.db import models

class Address(models.Model):
    """
        model for the address used in other models
    """
    id = models.AutoField(primary_key=True)
    street_name = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    postal_code = models.IntegerField()

    def __str__(self):
        return self.street_name
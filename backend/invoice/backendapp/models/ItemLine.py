from django.db import models
from rest_framework.fields import JSONField

from backendapp.models.AccountLine import AccountLine
from backendapp.models.Invoice import Invoice
from backendapp.models.Item import Item


class ItemLine(models.Model):
    """
         model for the item lines
    """
    id = models.AutoField(primary_key=True)
    item_JSON = models.CharField(max_length=1000, default="")
    item = models.ForeignKey(
        Item,
        on_delete=models.CASCADE,
        null=True
    )
    quantity = models.IntegerField()
    invoice = models.ForeignKey(
        Invoice,
        on_delete=models.CASCADE,
        null=True,
    )
    account_line = models.ForeignKey(
        AccountLine,
        on_delete=models.CASCADE,
        null=True
    )
    objects = models.Manager()

    def __str__(self):
        return self.item.description

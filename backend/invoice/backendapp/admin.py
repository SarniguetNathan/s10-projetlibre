from django.contrib import admin

# Register your models here.
from django.contrib.auth.admin import UserAdmin

from backendapp.models import *

admin.site.register(User)
admin.site.register(AccountLine)
admin.site.register(Address)
admin.site.register(Client)
admin.site.register(Invoice)
admin.site.register(Item)
admin.site.register(ItemLine)


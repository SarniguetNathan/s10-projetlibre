# S10-ProjetLibre
## Download project

Download the project (git clone or zip download) and open a cmd prompt at its base directory (you should see the README.md in this directory)

## Requirements

1. Create a Python 3 virutal environment

```Shell
Windows\S10-projetlibre\backend python -m venv venv
```

2. Activate the venv

```Shell
Windows\S10-projetlibre\backend venv\Scripts\activate.bat
```

3. Install the requirements

```Shell
(venv) Windows\S10-projetlibre\backend pip install -r requirements.txt
```

## Setup & Launch

1. Make migrations
```Shell
(venv) Windows\S10-projetlibre\backend\invoice$ python manage.py makemigrations
```

2. Migrate DB
```Shell
(venv) Windows\S10-projetlibre\backend\invoice$ python manage.py migrate
```

3. Run server
```Shell
(venv) Windows\S10-projetlibre\backend\invoice$ python manage.py runserver
```

## Usage

After launching the frontend : 

1. Go to http://localhost:8080/ and use the username user and password user


## Structure
Routes
```Shell
S10-projetlibre\backend\invoice\urls.py
```

Views( controllers )
```Shell
S10-projetlibre\backend\invoice\\app\views\
```
Models
```Shell
S10-projetlibre\backend\invoice\app\models\
```

Templates ( html )
```Shell
S10-projetlibre\backend\invoice\app\template\
```

Authentication system
```Shell
S10-projetlibre\backend\invoice\app\authentication\
```

import axios from 'axios'
import Vue from 'vue'
import router from './router'

let baseURL;

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  baseURL = 'http://localhost:8000/'
} else {
  baseURL = 'http://api.example.com'
}

Vue.prototype.$http = axios;
/**
 * Create an axios instance
 * @returns {AxiosInstance}
 */
export function getAxiosInstance() {
  console.log("coucou2")
  let accessToken = localStorage.getItem('access_token')
  let axiosInstance = axios.create(
    {
      baseURL: baseURL,
      headers: {'Authorization':'Bearer '+accessToken},
      crossDomain: true
    });

  axiosInstance.interceptors.response.use((response) => {
    return response
  }, (error) => {
    //if (error.response.status === 401) {
      //localStorage.setItem('access_token', '');
      //return router.push({name: 'Login'})
    //}
    return Promise.reject(error.message)
  });
  return axiosInstance
}
/**
 * Axios instance used to make request to the backend
 * @type {AxiosInstance}
 */
export var HTTP = getAxiosInstance();

import {HTTP} from '../http-constants'
/**
 * Communicate with the backend and allow a user to authenticate
 */
export class LoginRepository {

  /**
   * Authenticate a user
   * @param login login of the user
   * @param password password of the user
   * @returns {Promise<boolean>}
   */
  static login (login, password) {
    return HTTP.post('/api/token/', {'username': login, 'password': password})
      .then(jsonResponse => {
        localStorage.setItem('access_token', jsonResponse.data.access)
        return true
      })
  }

  /**
   * logout the user
   * @returns {boolean}
   */
  static logout () {
    localStorage.setItem('access_token', '')
    return true
  }
}

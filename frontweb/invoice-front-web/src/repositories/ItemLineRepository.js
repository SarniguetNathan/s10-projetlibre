import {getAxiosInstance, HTTP} from '../http-constants'
import {Client} from "../models/Client";
import {Address} from "../models/Address";
import {ItemLine} from "../models/ItemLine";

/**
 * Communicate with the backend and do CRUD operation on ItemLines
 */
export class ItemLineRepository {

  /**
   * create an itemLine for the authenticated user
   * @param itemLine itemLine to create
   * @returns {Promise<ItemLine>}
   */
  static create (itemLine) {
    if(itemLine.invoice && itemLine.invoice!=="")
      itemLine.invoice_id = itemLine.invoice.id
    return getAxiosInstance().post('/itemlines/', itemLine )
      .then(jsonResponse => {
        return ItemLine.from(jsonResponse.data)
      })
  }

  /**
   * Update an item line
   * @param itemLine itemline to update
   * @returns {Promise<ItemLine>}
   */
  static update(itemLine) {
    if(itemLine.invoice && itemLine.invoice!=="")
      itemLine.invoice_id = itemLine.invoice.id
    return getAxiosInstance().put('/itemlines/'+itemLine.id+'/', itemLine )
      .then(jsonResponse => {
        return ItemLine.from(jsonResponse.data)
      })
  }

  /**
   * get an itemline
   * @param itemLineId id of the itemline
   * @returns {Promise<ItemLine>}
   */
  static get(itemLineId) {
    return getAxiosInstance().get('/itemlines/'+itemLineId )
      .then(jsonResponse => {
        return ItemLine.from(jsonResponse.data)
      })
  }

}

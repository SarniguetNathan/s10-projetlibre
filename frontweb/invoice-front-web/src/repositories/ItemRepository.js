import {getAxiosInstance} from "../http-constants";
import {Item} from "../models/Item";
/**
 * Communicate with the backend and do CRUD operation on items
 */
export class ItemRepository {

  /**
   * Create an item for the authenticated user
   * @param item item to create
   * @returns {Promise<Item>}
   */
  static create (item) {
    item.pre_tax_price = parseFloat(item.pre_tax_price).toFixed(2)
    item.tax = parseFloat(item.tax).toFixed(2)
    return getAxiosInstance().post('/items/', item )
      .then(jsonResponse => {
        return Item.from(jsonResponse.data)
      })
  }

  /**
   * Update an item
   * @param item item to update
   * @returns {Promise<Item>}
   */
  static update(item) {
    return getAxiosInstance().put('/items/'+item.id+'/', item )
      .then(jsonResponse => {
        return Item.from(jsonResponse.data)
      })
  }

  /**
   * get an item
   * @param itemId id of the item to get
   * @returns {Promise<Item>}
   */
  static get(itemId) {
    return getAxiosInstance().get('/items/'+itemId )
      .then(jsonResponse => {
        return Item.from(jsonResponse.data)
      })
  }

  /**
   * Lis all the items of the authenticated user
   * @returns {Promise<[]>}
   */
  static getAll() {
    return getAxiosInstance().get('/items/' )
      .then(jsonResponse => {
        let items = []
        jsonResponse.data.forEach(
          jsonItem=>{
            items.push(Item.from(jsonItem))
          }
        )
        return items
      })
  }
}

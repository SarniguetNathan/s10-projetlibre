import {getAxiosInstance} from "../http-constants";
import {Invoice} from "../models/Invoice";
/**
 * Communicate with the backend and do CRUD operation on invoices
 */
export class InvoiceRepository {

  /**
   * Create an invoice for the authenticated user
   * @param invoice
   * @returns {Promise<Invoice>}
   */
  static create (invoice) {
    return getAxiosInstance().post('/invoices/', invoice )
      .then(jsonResponse => {
        return Invoice.from(jsonResponse.data)
      })
  }

  /**
   * Update an invoice of the authenticated user
   * @param invoice invoice to update
   * @returns {Promise<Invoice>}
   */
  static update(invoice) {
    return getAxiosInstance().put('/invoices/'+invoice.id+'/', invoice )
      .then(jsonResponse => {
        return Invoice.from(jsonResponse.data)
      })
  }

  /**
   * Get an invoice of the authenticated user
   * @param invoiceId id of the invoice to get
   * @returns {Promise<Invoice>}
   */
  static get(invoiceId) {
    return getAxiosInstance().get('/invoices/'+invoiceId )
      .then(jsonResponse => {
        return Invoice.from(jsonResponse.data)
      })
  }

  /**
   * List all the invoice related to the specified client
   * @param clientId id of the client
   * @returns {Promise<[]>}
   */
  static getClientInvoices(clientId) {
    return getAxiosInstance().get('/clients/'+clientId+'/invoices/' )
      .then(jsonResponse => {
        let invoices = []
        jsonResponse.data.forEach(
          jsonInvoice=>{
            invoices.push(Invoice.from(jsonInvoice))
          }
        )
        return invoices
      })
  }

  /**
   * Lis all the invoices of the authenticated user
   * @returns {Promise<[]>}
   */
  static getAll() {
    return getAxiosInstance().get('/invoices/' )
      .then(jsonResponse => {
        let invoices = []
        jsonResponse.data.forEach(
          jsonInvoice=>{
            invoices.push(Invoice.from(jsonInvoice))
          }
        )
        return invoices
      })
  }
}

import {getAxiosInstance, HTTP} from '../http-constants'
import {Client} from "../models/Client";
import {Address} from "../models/Address";
import {AccountLine} from "../models/AccountLine";
/**
 * Communicate with the backend and do CRUD operation on accountlines
 */
export class AccountLineRepository {

  /**
   * Create an accountline
   * @param accountLine accountline to create for the authenticated user
   * @returns {Promise<AccountLine>}
   */
  static create (accountLine) {
    accountLine.client_id = accountLine.client.id
    let totalAmount = 0
    let taxAmount = 0

    accountLine.item_lines.forEach(
      item_line =>{
        totalAmount += item_line.item.pre_tax_price * item_line.quantity
        taxAmount += item_line.item.pre_tax_price* item_line.item.tax * item_line.quantity
      }
    )

    accountLine.pre_tax_total = totalAmount.toFixed(2)
    accountLine.tax_amount = taxAmount.toFixed(2)
    return getAxiosInstance().post('/accountlines/', accountLine )
      .then(jsonResponse => {

        return AccountLine.from(jsonResponse.data)
      })
  }

  /**
   * update an accountline of the authenticated user
   * @param accountLine accountline to update
   * @returns {Promise<AccountLine>}
   */
  static update(accountLine) {
    return getAxiosInstance().put('/accountlines/'+accountLine.id+'/', accountLine )
      .then(jsonResponse => {
        return AccountLine.from(jsonResponse.data)
      })
  }

  /**
   * get an accountline of the authenticated user
   * @param accountLineId id of the accountline to get
   * @returns {Promise<AccountLine>}
   */
  static get(accountLineId) {
    return getAxiosInstance().get('/accountlines/'+accountLineId )
      .then(jsonResponse => {
        return AccountLine.from(jsonResponse.data)
      })
  }

  /**
   * List all accountLines of the authenticated user
   * @returns {Promise<[]>}
   */
  static getAll() {
    return getAxiosInstance().get('/accountlines/' )
      .then(jsonResponse => {
        let accountlines = []
        jsonResponse.data.forEach(
          jsonAccountLine=>{
            accountlines.push(AccountLine.from(jsonAccountLine))
          }
        )
        return accountlines
      })
  }
}

import {getAxiosInstance, HTTP} from '../http-constants'
import {User} from "../models/User";
/**
 * Communicate with the backend and do CRUD operation on Users
 */
export class UserRepository {

  /**
   * Update the authenticated user
   * @param user user to update
   * @returns {Promise<User>}
   */
  static update(user) {
    return getAxiosInstance().put('/users/', user )
      .then(jsonResponse => {
        return User.from(jsonResponse.data)
      })
  }

  /**
   * Get then authenticated user
   * @returns {Promise<User>}
   */
  static get() {
    return getAxiosInstance().get('/users/')
      .then(jsonResponse => {
        console.log(jsonResponse)
        return User.from(jsonResponse.data)
      })
  }

}

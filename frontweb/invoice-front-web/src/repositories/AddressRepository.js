import {getAxiosInstance, HTTP} from '../http-constants'
import {Client} from "../models/Client";
import {Address} from "../models/Address";

/**
 * Communicate with the backend and do CRUD operation on addresses
 */
export class AddressRepository {

  /**
   * Create an address
   * @param address address to create
   * @returns {Promise<Address>}
   */
  static create (address) {
    return getAxiosInstance().post('/addresses/', address )
      .then(jsonResponse => {
        return Address.from(jsonResponse.data)
      })
  }

  /**
   * Update an address
   * @param address address to update
   * @returns {Promise<Address>}
   */
  static update(address) {
    return getAxiosInstance().put('/addresses/'+address.id+'/', address )
      .then(jsonResponse => {
        return Address.from(jsonResponse.data)
      })
  }

  /**
   * Get an address
   * @param addressId id of the address to get
   * @returns {Promise<Address>}
   */
  static get(addressId) {
    return getAxiosInstance().get('/addresses/'+addressId )
      .then(jsonResponse => {
        return Address.from(jsonResponse.data)
      })
  }

}

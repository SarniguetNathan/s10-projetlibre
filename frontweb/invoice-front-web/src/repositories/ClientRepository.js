import {getAxiosInstance, HTTP} from '../http-constants'
import {Client} from "../models/Client";
/**
 * Communicate with the backend and do CRUD operation on Clients
 */
export class ClientRepository {

  /**
   * Create a client for the authenticated user
   * @param client client to create
   * @returns {Promise<Client>}
   */
  static create (client) {
    if(client.company_name  && client.company_name !== ""  )
      client.is_company = true
    else
      client.is_company = false
    return getAxiosInstance().post('/clients/', client )
      .then(jsonResponse => {
        return Client.from(jsonResponse.data)
      })
  }

  /**
   * Update a client of the authenticated user
   * @param client client to update
   * @returns {Promise<Client>}
   */
  static update(client) {
    if(client.company_name  && client.company_name !== ""  )
      client.is_company = true
    else
      client.is_company = false
    return getAxiosInstance().put('/clients/'+client.id+'/', client )
      .then(jsonResponse => {
        return Client.from(jsonResponse.data)
      })
  }

  /**
   * Get a client of the authenticated user
   * @param clientId id of the client to get
   * @returns {Promise<Client>}
   */
  static get(clientId) {
    return getAxiosInstance().get('/clients/'+clientId +'/')
      .then(jsonResponse => {
        console.log(jsonResponse)
        return Client.from(jsonResponse.data)
      })
  }

  /**
   * List all clients of the authenticated user
   * @returns {Promise<[]>}
   */
  static getAll() {
    return getAxiosInstance().get('/clients/' )
      .then(jsonResponse => {
        let clients = []
        jsonResponse.data.forEach(
          jsonClient=>{
            clients.push(Client.from(jsonClient))
          }
        )
        return clients
      })
  }
}

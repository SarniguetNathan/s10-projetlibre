
/**
 * Class representing an Item line, related to an item with a quantity
 */
export class ItemLine{
  /** @type {BigInteger}
   * @description id of the itemLine */
  id
  /** @type {BigInteger}
   * @description id of the related item */
  item_id
  /** @type {Item}
   * @description related item */
  item
  /** @type {BigInteger}
   * @description quantity  */
  quantity
  /** @type {number}
   * @description id of the related invoice, -1 if not related */
  invoice_id = -1
  /** @type {Array<ItemLine>}
   * @description  */
  item_lines
  /** @type {BigInteger}
   * @description related account line id */
  account_line_id

  constructor() {
  }
  static from(json){
    return Object.assign(new ItemLine(), json)
  }
}

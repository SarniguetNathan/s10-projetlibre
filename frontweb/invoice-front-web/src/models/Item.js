/**
 * Class representing a service or a product
 */
export class Item{
  /** @type {BigInteger}
   * @description id of the item */
  id
  /** @type {String}
   * @description name of the item */
  name
  /** @type {String}
   * @description reference of the item */
  ref
  /** @type {Number}
   * @description pre tax porice of the item */
  pre_tax_price
  /** @type {Number}
   * @description tax in between 0 and 1 */
  tax
  /** @type {String}
   * @description description of the item */
  description

  /**
   * Constructor
   */
  constructor() {
  }

  /**
   * Convert json to Item object
   * @param json
   * @returns {Item}
   */
  static from(json){
    return Object.assign(new Item(), json)
  }
}

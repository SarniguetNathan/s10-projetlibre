
import {Client} from "./Client"

/**
 * Class representing an invoice
 */
export class Invoice{
  /** @type {BigInteger}
   * @description id of the invoice */
  id
  /** @type {BigInteger}
   * @description number of the Invoice */
  invoice_number
  /** @type {Date}
   * @description date of creation */
  creation_date
  /** @type {Date}
   * @description effective date */
  effective_date
  /** @type {BigInteger}
   * @description id of the Client */
  client_id
  /** @type {Client}
   * @description related client */
  client = new Client()
  /** @type {String}
   * @description ref of the order */
  order_number

  /** @type {Number}
   * @description reduction */
  reduction
  /** @type {Date}
   * @description Date of paiement */
  pay_date
  /** @type {Number}
   * @description interest rate (if payment is late) */
  interest_rate
  /** @type {BigInteger}
   * @description payment delay */
  payment_delay
  /** @type {Array<ItemLine>}
   * @description item line list */
  item_lines
  /** @type {String}
   * @description description of the invoice */
  description


  /**
   * Constructor
   */
  constructor() {
  }

  /**
   * Convert json to Invoice object
   * @param json
   * @returns {Invoice}
   */
  static from(json){
    let invoice = Object.assign(new Invoice(), json)
    invoice.client =  Client.from(invoice.client)
    return invoice
  }
}

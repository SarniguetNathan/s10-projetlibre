import {Address} from "./Address";
/**
 * Class representing an invoice
 */
export class User{
  /** @type {BigInteger}
   * @description id of the invoice */
  id
  firstname
  lastname
  company_name
  address = new Address()
  social_denomination
  siret
  is_merchant
  RCS
  RCSCity
  is_artisant
  artisant_number
  company_type
  capital
  email
  password
  TVA_number
  insurance_mention
  insurance_address = new Address()
  geographic_cover
  is_insurance
  is_BTP
  is_association_or_gestion

  /**
   * Constructor
   */
  constructor() {

  }


  /**
   * Convert json to Invoice object
   * @param json
   * @returns {Invoice}
   */
  static from(json){
    return Object.assign(new User(), json)
  }

}


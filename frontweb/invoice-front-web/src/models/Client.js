import {Address} from "./Address";

/**
 * Class representing a client
 */
export class Client{
  /** @type {BigInteger}
   * @description id of the Client */
  id
  /** @type {Boolean}
   * @description true if the client is a company */
  is_company
  /** @type {string}
   * @description firstname of the client*/
  first_name
  /** @type {BigInteger}
   * @description lastname of the client */
  last_name
  /** @type {String}
   * @description company name of the client */
  company_name
  /** @type {Address}
   * @description address of the client */
  address= new Address()
  /** @type {BigInteger}
   * @description address id of the client */
  address_id
  /** @type {Address}
   * @description invoice address of the client */
  invoice_address = new Address()
  /** @type {BigInteger}
   * @description invoice address id of the client */
  invoice_address_id

  /**
   * Constructor
   */
  constructor() {
    this.is_company = false
  }

  /**
   * Convert json to AccountLine object
   * @param json
   * @returns {AccountLine}
   */
  static from(json){
    return Object.assign(new Client(), json)
  }
}

/**
 * Class representing an address
 */
export class Address{
  /** @type {BigInteger}
   * @description id of the address */
  id
  /** @type {string}
   * @description number and name of the street */
  street_name
  /** @type {BigInteger}
   * @description postal code */
  postal_code
  /** @type {string}
   * @description city */
  city

  /**
   * Constructor
   */
  constructor() {
  }

  /**
   * Convert json to Address object
   * @param json
   * @returns {Address}
   */
  static from(json){
    return Object.assign(new Address(), json)
  }
}

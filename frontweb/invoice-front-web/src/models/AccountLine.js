/**
 * Class representing an account line
 */
export class AccountLine{
  /** @type {BigInteger}
   * @description id of the accountline */
  id
  /** @type {String}
   * @description descritpion of the accountline */
  description
  /** @type {String}
   * @description date the accountline */
  account_line_date
  /** @type {BigInteger}
   * @description related client id */
  client_id
  /** @type {Client}
   * @description related client */
  client
  /** @type {BigInteger}
   * @description related invoice */
  invoice_id =-1
  /** @type {Array<ItemLine>}
   * @description list of item line */
  item_lines
  /** @type {Number}
   * @description pre tax total price */
  pre_tax_total
  /** @type {Number}
   * @description tax amoun*/
  tax_amount

  /**
   * Constructor
   */
  constructor() {
  }

  /**
   * Convert json to AccountLine object
   * @param json
   * @returns {AccountLine}
   */
  static from(json){
    return Object.assign(new AccountLine(), json)
  }

}

import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/Login'
import Invoices from "../components/Invoice/Invoices";
import ClientCreation from "../components/Client/ClientCreation";
import Clients from "../components/Client/Clients";
import ClientUpdate from "../components/Client/ClientUpdate";
import Items from "../components/Item/Items";
import InvoiceCreation from "../components/Invoice/InvoiceCreation";
import Account from "../components/Account/Account";
import User from "../components/User/User";

Vue.use(Router)

var router = new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/invoices',
      name: 'Invoices',
      component: Invoices
    },
    {
      path: '/invoicecreation',
      name: 'InvoiceCreation',
      component: InvoiceCreation
    },
    {
      path: '/clientcreation',
      name: 'ClientCreation',
      component: ClientCreation,
      props: true
    },
    {
      path: '/updateclient',
      name: 'UpdateClient',
      component: ClientUpdate,
      props: true
    },
    {
      path: '/clients',
      name: 'Clients',
      component: Clients
    },
    {
      path: '/items',
      name: 'Items',
      component: Items
    },
    {
      path: '/account',
      name: 'Account',
      component: Account
    },
    {
      path: '/user',
      name: 'User',
      component: User
    },

  ]
})



router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    const access_token = window.localStorage.getItem('access_token')
    if (access_token) {
      next()
    } else {
      next({name: 'Login'})
    }
  }
  next()
})
export default router
